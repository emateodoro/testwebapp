# Test Web App

This is a test application created in order to demo a few java technologies and libraries.

The following libraries / frameworks were included:

- SpringBoot (Web + JPA + Security)
- Thymeleaf
- Lombok

## Requirements

In order to build and run the application locally, you will need the following:


- Java 8+ (JDK)
- Maven
- MySQL or MariaDB installed and running
- Test Web Service configured and running (more info [here](https://gitlab.com/emateodoro/testdevws))

If you would prefer to run a pre-built container, you can pull from the public docker hub repo:

```
registry.hub.docker.com/etsousa/testwebapp:latest
```

## Environment Variables and Configuration

The following variables can be set in order to customize the app

| Name          | Default       |
| ------------- |:-------------:|
| APP_PORT      | 8080          |
| MYSQL_HOST    | localhost     |
| MYSQL_PORT    | 3306          |
| MYSQL_DBNAME  | testwebapp    |
| MYSQL_USERNAME| root          |
| MYSQL_PASSWORD| root          |
| WS_BASE_URL   | http://localhost:8080/services/user|




## Endpoints

Considering the default configurations, you have the following endpoints available:


### Pages

| URL           | Description   |
| ------------- |:-------------:|
| /login        |  Default page for unauthorized users |
| /user-view    |  User view page    |
| /admin-view    | Admin view page |
| /admin-user-view/{username}    | Admin view page for a specific user |


### Rest Endpoints (test api)

| URL           | Description   | Method |
| ------------- |:-------------:| :--: |
| /test/users        |  Returns a JSON with all users | GET |
| /test/user/{username}        |  Returns a JSON with one user | GET |
| /items-user/{username}        |  Returns a JSON with the items related to one user (calls backend ws)| GET |


