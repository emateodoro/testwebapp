package com.example.testwebapp.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.example.testwebapp.context.config.LoginSuccessHandler;
import com.example.testwebapp.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityContext extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	@Autowired
	private LoginSuccessHandler successHandler;

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.authorizeRequests()
					.antMatchers("/resources/**", "/webjars/**", "/lib/**", "/actuator/**", "/test/**").permitAll()
					.antMatchers("/login").permitAll()
					.antMatchers("/logout").permitAll()
					.antMatchers("/login-error").permitAll()
					.antMatchers("/user-view").hasAnyAuthority("Guest", "Admin")
					.antMatchers("/admin-view").hasAnyAuthority("Admin")
					.antMatchers("/admin-user-view/**").hasAnyAuthority("Admin")
					.antMatchers("/fragments/user-table").hasAnyAuthority("Admin").anyRequest()
					.fullyAuthenticated()
				.and()
					.formLogin()
					.loginPage("/login")
					.successHandler(successHandler)
					.usernameParameter("username")
					.passwordParameter("password")
					.failureUrl("/login-error")
					.permitAll()
				.and()
					.logout()
					.invalidateHttpSession(true)
					.clearAuthentication(true)
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
					.logoutSuccessUrl("/login")
					.permitAll()
				.and()
					.exceptionHandling()
					.accessDeniedPage("/user-view");
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(userService);
		provider.setPasswordEncoder(passwordEncoder());
		auth.authenticationProvider(provider);
	}

}
