package com.example.testwebapp.model.external;

import java.sql.Date;
import java.util.List;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName = "item")
public class ItemDto {
	
	private Long id;
	private String name;
	private String game;
	private Date expirationDate;
	private Integer quantity;
	
	@JacksonXmlProperty(localName = "property")
    @JacksonXmlElementWrapper(useWrapping = false)
	private List<PropertyDto> properties;
}
