package com.example.testwebapp.model.external;

import java.util.List;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName = "user")
public class ExternalUserDto {
	
	private Long id;
	private String username;

	@JacksonXmlProperty(localName = "item")
    @JacksonXmlElementWrapper(useWrapping = false)
	private List<ItemDto> items;
}