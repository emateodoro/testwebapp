package com.example.testwebapp.service;

import java.util.List;

import com.example.testwebapp.model.external.ItemDto;

public interface ItemService {
	
	public List<ItemDto> getItemsByUsername(String username);
}
