package com.example.testwebapp.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.example.testwebapp.model.UserDto;

public interface UserService extends UserDetailsService {
	
	public UserDto getUserByUsername(String username);
	
	public List<UserDto> getAllUsers();
}
