package com.example.testwebapp.service;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.example.testwebapp.model.external.ExternalUserDto;
import com.example.testwebapp.model.external.ItemDto;

@Service
public class ItemServiceImpl implements ItemService {

	private final Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);

	@Value("${devws.base-url}")
	private String webServiceUrl;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<ItemDto> getItemsByUsername(String username) {

		if (username != null && !username.isEmpty()) {
			try {
				ExternalUserDto response = restTemplate.getForObject(webServiceUrl + "/" + username,
						ExternalUserDto.class);

				if (response != null) {
					return response.getItems();
				}

			} catch (RestClientException ex) {
				logger.error(ex.getMessage(), ex);
			}
		}

		return Collections.emptyList();
	}

}
