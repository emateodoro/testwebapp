package com.example.testwebapp.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.testwebapp.model.RoleDto;
import com.example.testwebapp.model.UserDto;
import com.example.testwebapp.persistence.entity.Role;
import com.example.testwebapp.persistence.entity.User;
import com.example.testwebapp.persistence.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public UserDto getUserByUsername(String username) {
		UserDto userDto = null;
		
		if(username != null && !username.isEmpty()) {
			
			User user = userRepository.findByUsername(username);
			
			if(user != null) {
				userDto = convertUserToDto(user);
			}
		}
		
		return userDto;
	}
	
	@Override
	public List<UserDto> getAllUsers() {
		List<User> users = userRepository.findAll();
		
		return users.stream()
				.map(this::convertUserToDto)
				.collect(Collectors.toList());
	}

	private UserDto convertUserToDto(User user) {
		UserDto userDto = modelMapper.map(user, UserDto.class);
		
		Role role = user.getRole();
		
		if(role != null) {
			userDto.setRole(modelMapper.map(role, RoleDto.class));
		}
		
		return userDto;
	}

	@Override
	public UserDetails loadUserByUsername(String username) {

		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}

		return new org.springframework.security.core.userdetails.User(user.getUsername(), passwordEncoder.encode(user.getPassword()),
				Arrays.asList(new SimpleGrantedAuthority(user.getRole().getDescription())));
	}
	
}
