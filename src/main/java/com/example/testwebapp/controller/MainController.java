package com.example.testwebapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.testwebapp.service.ItemService;
import com.example.testwebapp.service.UserService;

@Controller
public class MainController {
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login";
	}
	
	@GetMapping("/index")
	public String index() {
		return "index";
	}
	
	@GetMapping("/user-view")
	public String userView(Model model) {
		return "/views/user-view";
	}
	
	@GetMapping("/admin-view")
	public String adminView(Model model) {
		return "/views/admin-view";
	}
	
	@GetMapping("/admin-user-view/{username}")
	public String adminUserView(Model model, @PathVariable("username") String username) {
		
		model.addAttribute("items", itemService.getItemsByUsername(username));
		
		return "/views/admin-user-view";
	}
	
	@GetMapping("/fragments/item-table")
	public String itemTable(Model model) {
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		model.addAttribute("items", itemService.getItemsByUsername(username));
		
		return "/fragments/item-table :: item-table";
	}
	
	@GetMapping("/fragments/user-table")
	public String userTable(Model model) {
		
		model.addAttribute("users", userService.getAllUsers());
		
		return "/fragments/user-table :: user-table";
	}
}
