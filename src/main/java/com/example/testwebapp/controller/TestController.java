package com.example.testwebapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.testwebapp.model.UserDto;
import com.example.testwebapp.model.external.ItemDto;
import com.example.testwebapp.service.ItemService;
import com.example.testwebapp.service.UserService;


@RestController
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	private UserService userService;
	

	@Autowired
	private ItemService itemService;

	@GetMapping(value = "/user/{username}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<UserDto> getUser(@PathVariable("username") String username) {
		UserDto userDto = userService.getUserByUsername(username);
		return new ResponseEntity<>(userDto, HttpStatus.OK);
	}
	
	@GetMapping(value = "/users", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<UserDto>> getUsers() {
		List<UserDto> usersDto = userService.getAllUsers();
		return new ResponseEntity<>(usersDto, HttpStatus.OK);
	}
	
	@GetMapping(value = "/items-user/{username}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<ItemDto>> getItemsUser(@PathVariable("username") String username) {
		List<ItemDto> usersDto = itemService.getItemsByUsername(username);
		return new ResponseEntity<>(usersDto, HttpStatus.OK);
	}
}
