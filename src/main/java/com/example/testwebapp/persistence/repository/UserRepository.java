package com.example.testwebapp.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.testwebapp.persistence.entity.User;


public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByUsername(String username);
}
