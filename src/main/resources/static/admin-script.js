$(document).ready(function () {
	var url = '/fragments/user-table';

    $("#userTableContainer").load(url);
})

function showUserItems(username) {
    var url = '/admin-user-view/' + username;

	window.location.replace(url);
}