package com.example.testwebapp.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import com.example.testwebapp.model.UserDto;
import com.example.testwebapp.persistence.entity.Role;
import com.example.testwebapp.persistence.entity.User;
import com.example.testwebapp.persistence.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	
	
	@Mock
	UserRepository userRepository;
	
	@Spy
	ModelMapper mapper = new ModelMapper();
	
	@InjectMocks
	UserService userService = new UserServiceImpl();
	
	
	@Before
	public void init() {
		List<User> mockedUsers = new ArrayList<>();
		User user1 = new User(1L, "user1", "pw", new Role(1L, "r1", null));
		User user2 = new User(2L, "user2", "pw", new Role(2L, "r2", null));
		mockedUsers.add(user1);
		mockedUsers.add(user2);
		
		Mockito.doReturn(mockedUsers).when(userRepository).findAll();
		Mockito.doReturn(user1).when(userRepository).findByUsername("user1");
	}
	
	@Test
	public void testAllUserMapping() {
		
		List<UserDto> users = userService.getAllUsers();
		
		Assert.assertNotNull(users);
		Assert.assertTrue(!users.isEmpty());
		Assert.assertEquals(2, users.size());
	}
	
	@Test
	public void testSingleUserMapping() {
		
		UserDto user = userService.getUserByUsername("user1");
		
		Assert.assertNotNull(user);
		Assert.assertEquals(1L, (long) user.getId());
		Assert.assertEquals("user1", user.getUsername());
		Assert.assertEquals("pw", user.getPassword());
		Assert.assertNotNull(user.getRole());
		Assert.assertEquals("r1", user.getRole().getDescription());
	}
	
	@Test
	public void testSingleUserNotFound() {
		
		UserDto user = userService.getUserByUsername("user3");
		
		Assert.assertNull(user);
	}
	
	@Test
	public void testSingleUserNullUsername() {
		
		UserDto user = userService.getUserByUsername(null);
		
		Assert.assertNull(user);
	}
	
	@Test
	public void testSingleUserEmptyUsername() {
		
		UserDto user = userService.getUserByUsername("");
		
		Assert.assertNull(user);
	}
}
