package com.example.testwebapp.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.example.testwebapp.model.external.ExternalUserDto;
import com.example.testwebapp.model.external.ItemDto;
import com.example.testwebapp.model.external.PropertyDto;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceTest {
	
	@Mock
	RestTemplate restTemplate;
	
	@InjectMocks
	ItemService itemService = new ItemServiceImpl();
	
	
	@Before
	public void init() {
		ReflectionTestUtils.setField(itemService, "webServiceUrl", "");
		ExternalUserDto mockedUser = new ExternalUserDto(1L, "user1", Arrays.asList(new ItemDto(1L, "name", "game1",null, 3, Arrays.asList(new PropertyDto(1L, "p1", "v1")))));
		
		Mockito.doReturn(mockedUser).when(restTemplate).getForObject(Mockito.anyString(), Mockito.any());
	}
	
	@Test
	public void testUserParsing() {
		List<ItemDto> items = itemService.getItemsByUsername("user1");

		Assert.assertNotNull(items);
		Assert.assertEquals(1, items.size());
		Assert.assertEquals("game1", items.get(0).getGame());
		Assert.assertEquals("name", items.get(0).getName());
		Assert.assertNotNull(items.get(0).getProperties());
		Assert.assertEquals(1, items.get(0).getProperties().size());
		Assert.assertEquals("p1", items.get(0).getProperties().get(0).getName());
		Assert.assertEquals("v1", items.get(0).getProperties().get(0).getValue());
	}
}
